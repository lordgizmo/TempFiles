import { TempDirectoryTests } from "./TempDirectory.test.js";
import { TempFileTests } from "./TempFile.test.js";
import { TempFileSystemTests } from "./TempFileSystem.test.js";

/**
 * Registers the tests.
 */
function Tests(): void
{
    suite(
        "TempFiles",
        () =>
        {
            TempFileSystemTests();
            TempFileTests();
            TempDirectoryTests();
        });
}

Tests();
